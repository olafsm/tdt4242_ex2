
describe('Register page boundary values', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Test1");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Navigates to gallery', () => {
    cy.visit('http://localhost:8000/gallery.html?id=21');
    cy.get(".main-img").find("img").should('have.attr', 'src', "http://127.0.0.1:9090/media/workouts/21/3x.png");
  });
  it('Opacity change on click', () => {
    cy.get("#img-collection-container").find("img").eq(1).click();
    cy.get("#img-collection-container").find("img").eq(1).should('have.css','opacity', '0.6');
  });
  it('Other images changes opacity back after click', () => {
    cy.get("#img-collection-container").find("img").eq(0).should('have.css','opacity', '1');
  });
});

