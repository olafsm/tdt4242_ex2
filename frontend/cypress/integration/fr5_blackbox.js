
describe('FR 5 Testing', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type('Test1');
    cy.get('[name=password]').type('Test2');
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Athlete should be able to view their own', () => {
    cy.visit('http://localhost:8000/workout.html?id=5');
    cy.contains('You do not have permission to perform this action').should('not.exist');
  });
  it('Owner should be able to view public', () => {
    cy.visit('http://localhost:8000/workout.html?id=7');
    cy.contains('You do not have permission to perform this action').should('not.exist');
  });

  it('Owner should not be able to view others private', () => {
    cy.visit('http://localhost:8000/workout.html?id=6');
    cy.contains('You do not have permission to perform this action').should('be.visible');
  });
  it('Owner should not be able to view others coach', () => {
    cy.visit('http://localhost:8000/workout.html?id=8');
    cy.contains('You do not have permission to perform this action').should('be.visible');
  });
  it('Coach should be able to see athletes coach workout', () => {
    cy.visit('http://localhost:8000/workout.html?id=4');
    cy.contains('You do not have permission to perform this action').should('not.exist');
  });
  it('Coach should be able to see athletes public workout', () => {
    cy.visit('http://localhost:8000/workout.html?id=10');
    cy.contains('You do not have permission to perform this action').should('not.exist');
  });
  it('Coach should not be able to see athletes private workout', () => {
    cy.visit('http://localhost:8000/workout.html?id=9');
    cy.contains('You do not have permission to perform this action').should('be.visible');
  });
  it('Signs in as visitor', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Visitor");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Visitor should not be able to view others private', () => {
    cy.visit('http://localhost:8000/workout.html?id=6');
    cy.contains('You do not have permission to perform this action').should('be.visible');
  });
  it('Visitor should not be able to view others coach', () => {
    cy.visit('http://localhost:8000/workout.html?id=8');
    cy.contains('You do not have permission to perform this action').should('be.visible');
  });

});
