// All fields with combinations of values to test
let A /* username */= ["Valid", "", "Special/¤'+2340"];
let B /* email */= ["valid@email.com", "invalid"];
let C /* password */= ["Password", ""];
let D /* password1 */= ["Password", ""];
let E /* phone_number */= ["12345689","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"];
let F /* country */= ["Nowray","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"];
let G /* city */= ["Oslo","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"];
let H /* street */= ["StreetName","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"];

describe('Everything is 1', ()=> {
  beforeEach(()=> {
    cy.wait(2000);
  })
  it('All 1', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=username]'      ).type(A[0]);
    cy.get('[name=email]'         ).type(B[0]);
    cy.get('[name=password]'      ).type(C[0]);
    cy.get('[name=password1]'     ).type(D[0]);
    cy.get('[name=phone_number]'  ).type(E[0]);
    cy.get('[name=country]'       ).type(F[0]);
    cy.get('[name=city]'          ).type(G[0]);
    cy.get('[name=street_address]').type(H[0]);
    cy.get('#btn-create-account').click();
    cy.contains('A user with that username already exists').should('be.visible');
  });

  it('A1 - Rest 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=username]'      ).type(A[0]);
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });

  it('A2-B1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[0]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });

  it('A2-C1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=password]'      ).type(C[0]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A2-D1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=password1]'     ).type(D[0]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A2-E1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[0]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A2-F1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[0]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A2-G1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[0]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A2-H1 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[0]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A3 REST 1', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=username]'      ).type(A[2]);
    cy.get('[name=email]'         ).type(B[0]);
    cy.get('[name=password]'      ).type(C[0]);
    cy.get('[name=password1]'     ).type(D[0]);
    cy.get('[name=phone_number]'  ).type(E[0]);
    cy.get('[name=country]'       ).type(F[0]);
    cy.get('[name=city]'          ).type(G[0]);
    cy.get('[name=street_address]').type(H[0]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });
  it('A3 REST 2', () => {
    cy.visit('http://localhost:8000/register.html');
    cy.get('[name=username]'      ).type(A[2]);
    cy.get('[name=email]'         ).type(B[1]);
    cy.get('[name=phone_number]'  ).type(E[1]);
    cy.get('[name=country]'       ).type(F[1]);
    cy.get('[name=city]'          ).type(G[1]);
    cy.get('[name=street_address]').type(H[1]);
    cy.get('#btn-create-account').click();
    cy.contains('Registration failed!').should('be.visible');
  });

});
