
beforeEach(() => {
  cy.visit('http://localhost:8000/register.html');
  cy.get('[name=username]').type("TestUsername");
  cy.get('[name=email]').type("test@email.com");
  cy.get('[name=password]').type("1234567890");
  cy.get('[name=password1]').type("1234567890");
  cy.get('[name=phone_number]').type("123456789");
  cy.get('[name=country]').type("No");
  cy.get('[name=city]').type("Trondheim");
  cy.get('[name=street_address]').type("Trondheim");
});


describe('Register page boundary values', () => {
  it('Checks username length', () => {
    cy.get('[name=username]').clear();
    cy.get('[name=username]').type("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    cy.get('#btn-create-account').click();
    cy.contains('Ensure this field has no more than 150 characters').should('be.visible');
  });
  it('Checks username for invalid characters', () => {
    cy.get('[name=username').clear();
    cy.get('[name=username]').type("89237]}}@[]£$][");
    cy.get('#btn-create-account').click();
    cy.contains('Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters').should('be.visible');
  });
  it('Checks that username is not blank', () => {
    cy.get('[name=username]').clear();
    cy.get('#btn-create-account').click();
    cy.contains('This field may not be blank').should('be.visible');
  });
  it('Checks that password is not blank', () => {
    cy.get('[name=password]').clear();
    cy.get('#btn-create-account').click();
    cy.contains('This field may not be blank').should('be.visible');
  });
  it('Checks that password1 is not blank', () => {
    cy.get('[name=password1]').clear();
    cy.get('#btn-create-account').click();
    cy.contains('This field may not be blank').should('be.visible');
  });
  it('Wrong email should throw error', () => {
    cy.get('[name=email]').clear();
    cy.get('[name=email]').type("89237]}}@[]£$][");
    cy.get('#btn-create-account').click();
    cy.contains('Enter a valid email address').should('be.visible');
  });
  it('Check email can not be too long', () => {
    cy.get('[name=email]').clear();
    cy.get('[name=email]').type("89237@aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.no");
    cy.get('#btn-create-account').click();
    cy.contains('Ensure this field has no more than 254 characters').should('be.visible');
  });
  it('Check email can not be blank', () => {
    cy.get('[name=email]').clear();
    cy.get('#btn-create-account').click();
    cy.contains('This field may not be blank').should('be.visible');
  });

});
