
describe('Myathlete offer regression test', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Test2");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Offer is sent correctly', () => {
    cy.visit('http://localhost:8000/myathletes.html');
    cy.get('input[name=athlete]').should('have.value', "Test2 (pending)");
  });
  it('Offer is recieved correctly', () => {
    cy.visit('http://localhost:8000/mycoach.html');
    cy.contains('Test2 wants to be your coach').should('be.visible');
  });
  it('Offer is accepted correctly', () => {
    cy.visit('http://localhost:8000/mycoach.html');
    cy.contains('Accept').click();
    cy.contains('Test2 wants to be your coach').should('not.exist');
  });
  it('Athlete is added correctly', () => {
    cy.visit('http://localhost:8000/myathletes.html');
    cy.get('input[name=athlete]').should('have.value', "Test2");
  });
});

