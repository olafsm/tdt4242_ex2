
describe('Register page boundary values', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Test1");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Navigates to exercises', () => {
    cy.visit('http://localhost:8000/exercises.html');
    cy.contains('View Exercises').should('be.visible');
  });
  it('Exercise does not save if cancel button is pressed', () => {
    cy.contains('Plank').click();
    cy.wait(1000);
    cy.get('#btn-edit-exercise').click();
    cy.get('#inputName').type("bvbbb");
    cy.get('#btn-cancel-exercise').click();
    cy.get('#inputName').should('not.contain.value', "bvbbb");
  });
});