Cypress.Cookies.debug(true);

describe('Signs in', () => {
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type('Test2');
    cy.get('[name=password]').type('Test1');
    cy.get('#btn-login').click();
  });
});

describe('Bunchaslihbasd', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8000/exercise.html?id=1');

    cy.wait(2000);
    cy.get('#btn-edit-exercise')
      .click({force: true});

    cy.get('#inputUnit').clear();
    cy.get('#inputUnit').type("15");
    cy.get('#inputDuration').clear();
    cy.get('#inputDuration').type("15");
    cy.get('#inputCalories').clear();
    cy.get('#inputCalories').type('15');

  });
  it('Tests for empty unit input', () => {
    cy.get('#inputUnit').clear();
    cy.get('#btn-ok-exercise').click();
    cy.contains('This field may not be blank').should('be.visible');
  });

  it('Tests for negative unit', () => {
    cy.get('#inputUnit').clear();
    cy.get('#inputUnit').type('-444');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });
  it('Tests for letters in unit input', () => {
    cy.get('#inputUnit').clear();
    cy.get('#inputUnit').type('adfasf');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });


  it('Tests for empty duration input', () => {
    cy.get('#inputDuration').clear();
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });
  it('Tests for letters in duration input', () => {
    cy.get('#inputDuration').clear();
    cy.get('#inputDuration').type('adfasf');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });

  it('Tests for negative duration input', () => {
    cy.get('#inputDuration').clear();
    cy.get('#inputDuration').type('-444');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });


  it('Tests for empty calories input', () => {
    cy.get('#inputCalories').clear();
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });
  it('Tests for negative calories input', () => {
    cy.get('#inputCalories').clear();
    cy.get('#inputCalories').type('-444');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });

  it('Tests for letters in calories input', () => {
    cy.get('#inputCalories').clear();
    cy.get('#inputCalories').type('adfasf');
    cy.get('#btn-ok-exercise').click();
    cy.contains('A valid integer is required').should('be.visible');
  });
});
