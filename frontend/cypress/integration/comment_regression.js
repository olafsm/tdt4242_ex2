describe('Comment regression tests', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false;
  });
    it('Signs in', () => {
      cy.visit('http://localhost:8000/login.html');
      cy.get('[name=username]').type("Test1");
      cy.get('[name=password]').type("Test2");
      cy.get('#btn-login').click();
      cy.contains('View Workouts').should('be.visible');
    });
    it('Navigates to gallery', () => {
      cy.visit('http://localhost:8000/workout.html?id=11');
      cy.get("#comment-area").type("CypressComment");
      cy.get("#post-comment").click();
      cy.contains("CypressComment").should('be.visible');
    });
    it('Signs in as other user', () => {
      cy.visit('http://localhost:8000/login.html');
      cy.get('[name=username]').type("Test2");
      cy.get('[name=password]').type("Test2");
      cy.get('#btn-login').click();
      cy.contains('View Workouts').should('be.visible');
    });
    it('Comment should not be visible', () => {
      cy.visit('http://localhost:8000/workout.html?id=11');
      cy.contains("CypressComment").should('not.exist');
    });
});

