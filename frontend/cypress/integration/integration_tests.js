
describe('Register page boundary values', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });
  it('Signs in', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Test1");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Test workout searchbar filters correctly', () => {
    cy.get('#searchWorkouts').clear();
    cy.get('#searchWorkouts').type('gsaasdgagsdsadg');
    cy.contains('Test1').should('not.be.visible');
  });
  it('Test workout searchbar gets existing', () => {
    cy.get('#searchWorkouts').clear();
    cy.get('#searchWorkouts').type('Test1');
    cy.contains('Test1').should('be.visible');
  });
  it('Create workout based on existing', () => {
    cy.contains('Test1').click();
    cy.contains('Create workout from this').click();
    cy.get('#inputName').clear();
    cy.get('#inputName').type("Workout created by cypress");
    cy.get('#btn-ok-workout').click();
    cy.contains('Workout created by cypress').should('be.visible');
  });
  it('Compare workout test', () => {
    cy.contains('Test1').click();
    cy.contains('Compare Workouts').click();
    cy.contains('Workout created by cypress').click();
    cy.get('#inputName2').should('be.visible');
  });
  it('Navigates to exercises', () => {
    cy.visit('http://localhost:8000/exercises.html');
    cy.contains('View Exercises').should('be.visible');
  });
  it('Test exercise searchbar filters correctly', () => {
    cy.get('#searchExercises').clear();
    cy.get('#searchExercises').type('gsaasdgagsdsadg');
    cy.contains('Plank').should('not.exist');
  });
  it('Test exercise searchbar gets existing', () => {
    cy.wait(1000);
    cy.get('#searchExercises').clear();
    cy.get('#searchExercises').type('Plank');
    cy.contains('Plank').should('be.visible');
  });

  it('Creates exercise based on existing', () => {
    cy.contains('Plank').click();
    cy.contains('Create New Exercise').click();
    cy.get('#inputName').type("Exercise created by cypress");
    cy.get('#btn-ok-exercise').click();
    cy.contains('PlankExercise created by cypress').should('be.visible');
  });
  it('Compares exercise', () => {
    cy.contains('Plank').click();
    cy.contains('Compare Exercises').click();
    cy.contains('PlankExercise created by cypress').click();
    cy.get('#inputName2').should('be.visible');
  });
  it('Visits workouts.html', () => {
    cy.visit('http://localhost:8000/workouts.html');
    cy.get("#btn-toggle-calendar").should('have.class', 'disabled');
  })
  it('Clicking my workouts enables button', () => {
    cy.wait(1000);
    cy.get("#list-tab").children().eq(1).click();
    cy.get("#btn-toggle-calendar").should('not.have.class', 'disabled');
  });
  it('Checks if workouts are added to calendar', () => {
    cy.get("#btn-toggle-calendar").click();
    cy.get(".fc-button-group").children().eq(1).click();
    cy.get(".fc-button-group").children().eq(0).click();
    cy.contains("Test1 - Test1").should('be.visible');
  });
  it('Signs in as coach', () => {
    cy.visit('http://localhost:8000/login.html');
    cy.get('[name=username]').type("Test2");
    cy.get('[name=password]').type("Test2");
    cy.get('#btn-login').click();
    cy.contains('View Workouts').should('be.visible');
  });
  it('Visits workouts.html as coach', () => {
    cy.visit('http://localhost:8000/workouts.html');
    cy.get("#btn-toggle-calendar").should('have.class', 'disabled');
  });

  it('Clicks Athlete workouts button', () => {
    cy.wait(1000);
    cy.get("#list-tab").children().eq(2).click();
    cy.get("#btn-toggle-calendar").should('not.have.class', 'disabled');
  });

  it('Checks if athletes workouts are added to calendar', () => {
    cy.get("#btn-toggle-calendar").click();
    cy.get(".fc-button-group").children().eq(1).click();
    cy.get(".fc-button-group").children().eq(0).click();
    cy.contains("Test1 - Test1").should('be.visible');
  });


});
