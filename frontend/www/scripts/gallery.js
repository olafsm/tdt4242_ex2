let goBackButton;

function populateImage(file, fileCounter) {
    let filesDiv = document.getElementById("img-collection");
    let filesDeleteDiv = document.getElementById("img-collection-delete");

    let deleteImgButton = document.createElement("input");
    deleteImgButton.type = "button";
    deleteImgButton.className = "btn btn-close";
    deleteImgButton.id = file.url.split("/")[file.url.split("/").length - 2];
    deleteImgButton.addEventListener('click', () => {
        handleDeleteImgClick(deleteImgButton.id);
    });
    filesDeleteDiv.appendChild(deleteImgButton);

    let img = document.createElement("img");
    img.src = file.file;

    filesDiv.appendChild(img);
    deleteImgButton.style.left = `${(fileCounter % 4) * 191}px`;
    deleteImgButton.style.top = `${Math.floor(fileCounter / 4) * 105}px`;
}

async function retrieveWorkoutImages(id) {
    let response = await sendRequest("GET", `${HOST}/api/workouts/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve workout data!", data);
        document.body.prepend(alert);
        return null;
    }

    let workoutData = await response.json();

    document.getElementById("workout-title").innerHTML = "Workout name: " + workoutData["name"];
    document.getElementById("workout-owner").innerHTML = "Owner: " + workoutData["owner_username"];

    let hasNoImages = workoutData.files.length === 0;
    let noImageText = document.querySelector("#no-images-text");

    if(hasNoImages){
        noImageText.classList.remove("hide");
        return;
    }

    noImageText.classList.add("hide");

    let isFirstImg = true;
    let fileCounter = 0;
    const currentImageFileElement = document.querySelector("#current");

    for (let file of workoutData.files) {
        let a = document.createElement("a");
        a.href = file.file;
        let pathArray = file.file.split("/");
        a.text = pathArray[pathArray.length - 1];
        a.className = "me-2";

        let isImage = ["jpg", "png", "gif", "jpeg", "JPG", "PNG", "GIF", "JPEG"].includes(a.text.split(".")[1]);

        if(isImage){
            if (isFirstImg) {
                isFirstImg = false;
                currentImageFileElement.src = file.file;
            }
            populateImage(file, fileCounter);
            fileCounter++;
        }
    }

    const otherImageFileElements = document.querySelectorAll(".imgs img");
    const selectedOpacity = 0.6;
    otherImageFileElements[0].style.opacity = selectedOpacity;

    otherImageFileElements.forEach((imageFileElement) =>
      imageFileElement.addEventListener("click", (event) => {
        //Changes the main image
        currentImageFileElement.src = event.target.src;

        //Adds the fade animation
        currentImageFileElement.classList.add('fade-in');
        setTimeout(() => currentImageFileElement.classList.remove('fade-in'), 500);

        //Sets the opacity of the selected image to selectedOpacity
        otherImageFileElements.forEach((imageFileElement2) => imageFileElement2.style.opacity = 1);
        event.target.style.opacity = selectedOpacity;
    }));
    return workoutData;
}

async function validateImgFileType(id) {
    let acceptedFileTypes = ["jpg", "png", "gif", "jpeg", "JPG", "PNG", "GIF", "JPEG"];
    let file = await sendRequest("GET", `${HOST}/api/workout-files/${id}/`);
    let fileData = await file.json();
    let fileType = fileData.file.split("/")[fileData.file.split("/").length - 1].split(".")[1];
    
    return acceptedFileTypes.includes(fileType);
}

async function handleDeleteImgClick (id) {
    if(await validateImgFileType(id)){
        return;
    }

    let response = await sendRequest("DELETE", `${HOST}/api/workout-files/${id}/`);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete workout ${id}!`, data);
        document.body.prepend(alert);
    } else {
        location.reload();
    }
}

function handleGoBackToWorkoutClick() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    window.location.replace(`workout.html?id=${id}`);
}

window.addEventListener("DOMContentLoaded", async () => {

    goBackButton = document.querySelector("#btn-back-workout");
    goBackButton.addEventListener('click', handleGoBackToWorkoutClick);

    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    await retrieveWorkoutImages(id);
});