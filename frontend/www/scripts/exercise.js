let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

function showEditButton() {
  okButton.className += " hide";
  deleteButton.className += " hide";
  cancelButton.className += " hide";
  editButton.className = editButton.className.replace(" hide", "");
}
function handleCancelButtonDuringEdit() {
  setReadOnly(true, "#form-exercise");
  document.querySelector("select").setAttribute("disabled", "");
  showEditButton();
  cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
  let form = document.querySelector("#form-exercise");

  [...form].forEach( field => {
    if (field.type !== "button") {
      field.value = oldFormData.get(field.name);
      oldFormData.delete(field.name);
    }
  });
}

function handleCancelButtonDuringCreate() {
  window.location.replace("exercises.html");
}

async function createExercise() {
  document.querySelector("select").removeAttribute("disabled");
  let form = document.querySelector("#form-exercise");
  let formData = new FormData(form);

  let body = Object.fromEntries(formData.entries());

  let response = await sendRequest("POST", `${HOST}/api/exercises/`, body);

  if (response.ok) {
    window.location.replace("exercises.html");
  } else {
    let data = await response.json();
    let alert = createAlert("Could not create new exercise!", data);
    document.body.prepend(alert);
  }
}

function handleEditExerciseButtonClick() {
  setReadOnly(false, "#form-exercise");

  document.querySelector("select").removeAttribute("disabled");

  editButton.className += " hide";
  okButton.className = okButton.className.replace(" hide", "");
  cancelButton.className = cancelButton.className.replace(" hide", "");
  deleteButton.className = deleteButton.className.replace(" hide", "");

  cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

  let form = document.querySelector("#form-exercise");
  oldFormData = new FormData(form);
}
//Group 38
function handleCompareExercisesButtonClick() {
  let exercises = document.querySelector("#div-content");
  exercises.className = exercises.className.replace("hide", "");
}
async function deleteExercise(id) {
  let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
  if (!response.ok) {
    let data = await response.json();
    let alert = createAlert(`Could not delete exercise ${id}`, data);
    document.body.prepend(alert);
  } else {
    window.location.replace("exercises.html");
  }
}

function handleNewExcerciseFromExistingButtonClick() {
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");
  window.location.replace(`create_exercise.html?exId=${id}`);
}

async function fetchExerciseTypes() {
  let response = await sendRequest("GET", `${HOST}/api/exercises/`);
  if (response.ok) {
    let data = await response.json();

    let exercises = data.results;
    let container = document.getElementById("div-content");
    let exerciseTemplate = document.querySelector("#template-exercise");
    exercises.forEach((exercise) => {
      const exerciseAnchor =
        exerciseTemplate.content.firstElementChild.cloneNode(true);
      exerciseAnchor.addEventListener("click", function (e) {
        handleCompareExerciseClick(`${exercise.id}`);
      });

      const h5 = exerciseAnchor.querySelector("h5");
      h5.textContent = exercise.name;

      const p = exerciseAnchor.querySelector("p");
      p.textContent = exercise.description;

      container.appendChild(exerciseAnchor);
    });
  }

  return response;
}

async function handleCompareExerciseClick(id) {
  let compare_form = document.querySelector("#form-exercise2");
  compare_form.className = compare_form.className.replace("hide", "");
  await retrieveExercise(id, false);
}

async function retrieveExercise(id, cmp) {
  let response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);

  console.log(response.ok);

  if (!response.ok) {
    let data = await response.json();
    let alert = createAlert("Could not retrieve exercise data!", data);
    document.body.prepend(alert);
  } else {
    document.querySelector("select").removeAttribute("disabled");
    let exerciseData = await response.json();
    let form = document.querySelector(
      cmp ? "#form-exercise" : "#form-exercise2"
    );
    let formData = new FormData(form);

    for (let key of formData.keys()) {
      let selector;
      if (key !== "muscleGroup") {
        selector = `input[name="${key}"], textarea[name="${key}"]`;
      } else {
        selector = `select[name=${key}]`;
      }

      let input = form.querySelector(selector);
      let newVal = exerciseData[key];
      input.value = newVal;
    }
    document.querySelector("select").setAttribute("disabled", "");
  }
}

async function updateExercise(id) {
  let form = document.querySelector("#form-exercise");
  let formData = new FormData(form);

  let muscleGroupSelector = document.querySelector("select");
  muscleGroupSelector.removeAttribute("disabled");

  let body = Object.fromEntries(formData.entries());
  let response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, body);

  if (!response.ok) {
    let data = await response.json();
    let alert = createAlert(`Could not update exercise ${id}`, data);
    document.body.prepend(alert);
  } else {
    muscleGroupSelector.setAttribute("disabled", "");
    // duplicate code from handleCancelButtonDuringEdit
    // you should refactor this
    setReadOnly(true, "#form-exercise");

    showEditButton();

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    [...form].forEach( field => {
      if (field.type !== "button") {
        oldFormData.delete(field.name);
      }
    });
  }
}

window.addEventListener("DOMContentLoaded", async () => {
  console.log("DOMContentLoaded");
  cancelButton = document.querySelector("#btn-cancel-exercise");
  okButton = document.querySelector("#btn-ok-exercise");
  deleteButton = document.querySelector("#btn-delete-exercise");
  editButton = document.querySelector("#btn-edit-exercise");
  oldFormData = null;

  const urlParams = new URLSearchParams(window.location.search);

  // view/edit
  if (urlParams.has("id")) {
    let response = await fetchExerciseTypes();

    if (!response.ok) {
      let data = await response.json();
      let alert = createAlert("Could not retrieve exercise types!", data);
      document.body.prepend(alert);
    }

    const exerciseId = urlParams.get("id");
    await retrieveExercise(exerciseId, true);

    editButton.addEventListener("click", handleEditExerciseButtonClick);
    deleteButton.addEventListener(
      "click",
      (async (id) => deleteExercise(id)).bind(undefined, exerciseId)
    );
    okButton.addEventListener(
      "click",
      (async (id) => updateExercise(id)).bind(undefined, exerciseId)
    );
  }
  //Group 38: create base on existing
  else {
    if (urlParams.has("exId")) {
      const exerciseId = urlParams.get("exId");
      await retrieveExercise(exerciseId, true);
    }
    setReadOnly(false, "#form-exercise");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");

    okButton.addEventListener("click", async () => createExercise());
    cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
  }
});
