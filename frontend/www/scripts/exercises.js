//Group 38: moved data fetching to the addEventListener on domcontentloaded
async function fetchExerciseTypes(data, request = null) {
  let exercises = data.results;
  let container = document.getElementById("div-content");
  let exerciseTemplate = document.querySelector("#template-exercise");
  container.innerHTML = "";
  if (request) {
    exercises = exercises.filter(
      (x) =>
        x.name.toLowerCase().includes(request.toLowerCase()) ||
        x.description.toLowerCase().includes(request.toLowerCase())
    );
  }
  exercises.forEach((exercise) => {
    const exerciseAnchor =
      exerciseTemplate.content.firstElementChild.cloneNode(true);
    exerciseAnchor.href = `exercise.html?id=${exercise.id}`;

    const h5 = exerciseAnchor.querySelector("h5");
    h5.textContent = exercise.name;

    const p = exerciseAnchor.querySelector("p");
    p.textContent = exercise.description;

    container.appendChild(exerciseAnchor);
  });
  return response;
}

function createExercise() {
  window.location.replace("create_exercise.html");
}

// group 38: Added search functionality
window.addEventListener("DOMContentLoaded", async () => {
  let createButton = document.querySelector("#btn-create-exercise");
  createButton.addEventListener("click", createExercise);

  let response = await sendRequest("GET", `${HOST}/api/exercises/`);
  let data;

  if (!response.ok) {
    data = await response.json();
    let alert = createAlert("Could not retrieve exercise types!", data);
    document.body.prepend(alert);
  } else if (response.ok) {
    data = await response.json();
    let searchExercises = document.querySelector("#searchExercises");
    searchExercises.addEventListener("input", (e) => {
      fetchExerciseTypes(data, e.target.value);
    });
    fetchExerciseTypes(data);
  }
});
