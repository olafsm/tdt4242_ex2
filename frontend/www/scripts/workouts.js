function fetchWorkouts(workouts) {
  let container = document.getElementById("div-content");
  container.innerHTML = "";
  workouts.forEach((workout) => {
    let templateWorkout = document.querySelector("#template-workout");
    let cloneWorkout = templateWorkout.content.cloneNode(true);

    let aWorkout = cloneWorkout.querySelector("a");
    aWorkout.href = `workout.html?id=${workout.id}`;

    let h5 = aWorkout.querySelector("h5");
    h5.textContent = workout.name;

    let localDate = new Date(workout.date);

    let table = aWorkout.querySelector("table");
    let rows = table.querySelectorAll("tr");
    rows[0].querySelectorAll("td")[1].textContent =
      localDate.toLocaleDateString(); // Date
    rows[1].querySelectorAll("td")[1].textContent =
      localDate.toLocaleTimeString(); // Time
    rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
    rows[3].querySelectorAll("td")[1].textContent =
      workout.exercise_instances.length; // Exercises

    container.appendChild(aWorkout);
  });
  return workouts;
}

function createWorkout() {
  window.location.replace("workout.html");
}

// Group 38: Gets a list of workouts and renders them in a calendar view
function renderCalendar(events) {
  let formattedCalendarWorkouts = [];
  events.forEach( (workout) => {
    formattedCalendarWorkouts.push(
      {
        title: workout.owner_username + " - " + workout.name,
        url: `workout.html?id=${workout.id}`,
        start:workout.date,
      }
    );
  });

  let calendarEl = document.querySelector('#calendar');
  calendarEl.innerHTML = "";
  let calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: [ 'interaction', 'dayGrid' ],
    defaultDate: '2022-03-13',
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: formattedCalendarWorkouts,
  });

  calendar.render();
}

window.addEventListener("DOMContentLoaded", async () => {
  let createButton = document.querySelector("#btn-create-workout");
  createButton.addEventListener("click", createWorkout);

  addCalendarButtonEventListeners();

  let ordering = "-date";

  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has("ordering")) {
    let aSort = null;
    ordering = urlParams.get("ordering");
    if (ordering == "name" || ordering == "owner" || ordering == "date") {
      let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
      aSort.href = `?ordering=-${ordering}`;
    }
  }

  let currentSort = document.querySelector("#current-sort");
  currentSort.innerHTML =
    (ordering.startsWith("-") ? "Descending" : "Ascending") +
    " " +
    ordering.replace("-", "");

  let currentUser = await getCurrentUser();
  // grab username
  if (ordering.includes("owner")) {
    ordering += "__username";
  }
  let response = await sendRequest(
    "GET",
    `${HOST}/api/workouts/?ordering=${ordering}`
  );
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  let data = await response.json();
  let workouts = data.results;
  fetchWorkouts(workouts);

  // Group 38: Gets a search term from the search bar and filters out non-matching workouts
  let searchWorkouts = document.querySelector("#searchWorkouts");
  let currentTab = "";
  searchWorkouts.addEventListener("input", (e) => {
    let filterdWorkouts = workouts.filter(
      (x) =>
        x.name.toLowerCase().includes(e.target.value.toLowerCase()) ||
        x.owner_username.toLowerCase().includes(e.target.value.toLowerCase())
    );
    fetchWorkouts(filterdWorkouts);
    filterByOwner(workouts, currentTab, currentUser);
  });

  let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');

  /* Group 38: Gets a list of all workouts, and filters them based on which tab is active
  * List is then sent to the renderCalendar function for rendering
   */
  let calendarWorkouts;
  calendarWorkouts = workouts.filter(
    (workout) => workout.owner === currentUser.url
  );

  for (let i = 0; i < tabEls.length; i++) {
    let tabEl = tabEls[i];
    tabEl.addEventListener("show.bs.tab", function (event) {
      currentTab = event.currentTarget.id;
      console.log("TabEventListener");
      console.log(currentTab);

      if(currentTab === "list-my-workouts-list") {
        calendarWorkouts = workouts.filter(
          (workout) => workout.owner === currentUser.url
        );
      } else if (currentTab === "list-athlete-workouts-list") {
        calendarWorkouts = workouts.filter(
          (workout) => (currentUser.athletes &&
            currentUser.athletes.includes(workout.owner)
          )
        );
      }
      renderCalendar(calendarWorkouts);
      filterByOwner(workouts, currentTab, currentUser);
    });
  }

  renderCalendar(calendarWorkouts);
});

function filterByOwner(workouts, currentTab, currentUser) {
  let workoutAnchors = document.querySelectorAll(".workout");
  let toggleCalendarButton = document.querySelector("#btn-toggle-calendar");

  for (let j = 0; j < workouts.length; j++) {
    // I'm assuming that the order of workout objects matches
    // the other of the workout anchor elements. They should, given
    // that I just created them.
    let workout = workouts[j];
    let workoutAnchor = workoutAnchors[j];

    switch (currentTab) {
      case "list-my-workouts-list":
        toggleCalendarButton.classList.remove("disabled");
        if (workout.owner === currentUser.url) {
          workoutAnchor.classList.remove("hide");
        } else {
          workoutAnchor.classList.add("hide");
        }
        break;
      case "list-athlete-workouts-list":
        toggleCalendarButton.classList.remove("disabled");
        if (
          currentUser.athletes &&
          currentUser.athletes.includes(workout.owner)
        ) {
          workoutAnchor.classList.remove("hide");
        } else {
          workoutAnchor.classList.add("hide");
        }
        break;
      case "list-public-workouts-list":
        toggleCalendarButton.classList.add("disabled");
        if (workout.visibility == "PU") {
          workoutAnchor.classList.remove("hide");
        } else {
          workoutAnchor.classList.add("hide");
        }
        break;
      default:
        toggleCalendarButton.classList.add("disabled");
        workoutAnchor.classList.remove("hide");
        break;
    }
  }
}

// Group 38: Toggles views of HTML elemnts on the workouts page based on if it should show list view or calendar view
function addCalendarButtonEventListeners() {
  let calendarHidden = true;
  let toggleCalendarButton = document.querySelector("#btn-toggle-calendar");
  let workoutsList = document.querySelector("#listViewContent");
  let workoutsCalendar = document.querySelector("#workoutsCalendar");
  let allWorkoutsTab = document.querySelector("#list-all-workouts-list");
  let publicWorkoutsTab = document.querySelector("#list-public-workouts-list");
  toggleCalendarButton.addEventListener("click", () => {
    if(calendarHidden) {
      workoutsList.classList.add("hide");
      workoutsCalendar.classList.remove("hide");
      calendarHidden = false;
      toggleCalendarButton.value = "Show list view";
      publicWorkoutsTab.classList.add("hide");
      allWorkoutsTab.classList.add("hide");

    } else {
      workoutsCalendar.classList.add("hide");
      workoutsList.classList.remove("hide");
      calendarHidden = true;
      toggleCalendarButton.value = "Show calendar view";
      publicWorkoutsTab.classList.remove("hide");
      allWorkoutsTab.classList.remove("hide");
    }
  });
}