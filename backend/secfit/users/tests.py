from django.test import TestCase

# Create your tests here.
from users.models import User
from users.serializers import UserSerializer


class UserSerializerTest(TestCase):
    def setUp(self):
        self.user_attributes = {
            'username': 'foo',
            'email': 'foo@foo.foo',
            'password': 'password',
            'phone_number': 'phone_number',
            'country': 'country',
            'city': 'city',
            'street_address': 'street_address'
        }
        self.serializer = UserSerializer()

    def user_has_all_attributes(self, user):
        for k in self.user_attributes.keys():
            if not hasattr(user, k):
                return False
        return True

    def test_create(self):
        user = self.serializer.create(self.user_attributes)

        self.assertTrue(self.user_has_all_attributes(user))