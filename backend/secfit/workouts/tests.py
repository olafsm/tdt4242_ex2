"""
Tests for the workouts application.
"""
from django.test import RequestFactory, TestCase
from users.models import User
# Create your tests here.
from workouts.permissions import *
from workouts.models import Exercise, ExerciseInstance, Workout
from django.utils import timezone

class MockObject(object):
    def __init__(self, owner):
        self.owner = owner


class MockObjectWorkout(object):
    def __init__(self, obj):
        self.workout = obj


class MockObjectCoach(object):
    def __init__(self, obj):
        self.coach = obj


class MockObjectVisibility(object):
    def __init__(self, obj):
        self.visibility = obj


class IsOwnerTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_object_permission_returns_false(self):
        request = self.factory.get('/')
        request.user = self.user
        owner_check = IsOwner()
        self.assertFalse(owner_check.has_object_permission(request, None, MockObject("nils")))

    def test_has_object_permission_returns_true(self):
        request = self.factory.get('/')
        request.user = self.user
        owner_check = IsOwner()
        self.assertTrue(owner_check.has_object_permission(request, None, MockObject(self.user)))


class IsOwnerOfWorkoutTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_permission_get_request_returns_true(self):
        request = self.factory.get('/')
        res = IsOwnerOfWorkout().has_permission(request, None)
        self.assertTrue(res)

    def test_has_permission_post_request_without_workout_returns_false(self):
        request = self.factory.post('/')
        request.data = {"workout": None}
        res = IsOwnerOfWorkout().has_permission(request, None)
        self.assertFalse(res)

    def test_has_permission_post_request_with_workout_returns_true(self):
        request = self.factory.post('/')
        request.data = {"workout": "01/"}
        request.user = self.user
        Workout.objects.create(
            owner=self.user, date=timezone.now()
        )
        res = IsOwnerOfWorkout().has_permission(request, None)
        self.assertTrue(res)

    def test_has_object_permission(self):
        obj = MockObjectWorkout(MockObject(self.user))
        request = self.factory.get("/")
        request.user = self.user
        res = IsOwnerOfWorkout().has_object_permission(request, None, obj)
        self.assertTrue(res)


class IsCoachAndVisibleToCoachTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_object_permission_returns_true(self):
        obj = MockObject(MockObjectCoach(self.user))
        request = self.factory.get("/")
        request.user = self.user
        res = IsCoachAndVisibleToCoach().has_object_permission(request, None, obj)
        self.assertTrue(res)


class IsCoachOfWorkoutAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_object_permission(self):
        obj = MockObjectWorkout(MockObject(MockObjectCoach(self.user)))
        request = self.factory.get("/")
        request.user = self.user
        res = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(
            request, None, obj
        )
        self.assertTrue(res)


class IsPublicTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_object_permission(self):
        obj = MockObjectVisibility("PU")
        request = self.factory.get("/")
        res = IsPublic().has_object_permission(request, None, obj)
        self.assertTrue(res)


class IsWorkoutPublicTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.factory = RequestFactory()

    def test_has_object_permission(self):
        obj = MockObjectWorkout(MockObjectVisibility("PU"))
        request = self.factory.get("/")
        res = IsWorkoutPublic().has_object_permission(request, None, obj)
        self.assertTrue(res)


class IsReadOnlyTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_has_object_permission(self):
        request = self.factory.get("/")
        obj = MockObject("foo")
        res = IsReadOnly().has_object_permission(request, None, obj)
        self.assertTrue(res)


class ExerciseInstanceCRUDTest(TestCase):
    def setUp(self):
        self.exercise = Exercise.objects.create(name="FooExercise", description="FooDescription", unit=32)
        self.user = User.objects.create(username="baz")
        self.workout = Workout.objects.create(name="FooWorkout", date=timezone.now(), owner=self.user)
        self.ex_instance = ExerciseInstance.objects.create(
            workout=self.workout,
            exercise=self.exercise,
            sets=2,
            number=2,
        )

    def test_update_and_create_exercise_instances(self):
        e = Exercise.objects.create(name="BarExercise", description="BarDescription", unit=32)
        ExerciseInstance.objects.update_or_create(pk=self.ex_instance.id, defaults={'exercise': e})
        self.assertEqual(ExerciseInstance.objects.get(pk=self.ex_instance.id).exercise.name, "BarExercise")